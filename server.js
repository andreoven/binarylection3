const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const users = require('./users.json');
const race = require('./race.json');
const {getGreeting} = require('./Announcer');
const {updateUsers} = require('./Watcher');
const {updateRegular} = require('./Watcher');
const {facade} = require('./Facade');

require('./passport.config');

server.listen(3000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

let currentRace = 0;
app.get('/race', passport.authenticate('jwt', { session: false }), function (req, res) {
  if (currentRace > 4){
    currentRace = 0;
  }
  res.send(race[currentRace]);
});

app.get('/login', function (req, res) {

});

app.post('/login', function (req, res) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'someSecret');
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});

let activeUsers = new Set();
let initialRaceStart = new Date();
let raceEnds;



io.on('connection', socket => {
  socket.on('verifyConnect', payload => {
    const user = jwt.verify(payload, 'someSecret');

    if (user) {
      if (activeUsers.size < 1){
        initialRaceStart.setSeconds(initialRaceStart.getSeconds() + 20);
        socket.emit('raceStartsIn', {time: initialRaceStart.getTime(), userlogin: user.login});
        socket.join('activeRacers');
        activeUsers.add(user.login);
      } else if (initialRaceStart.getTime() - new Date().getTime() > 0) {
        socket.emit('raceStartsIn', {time: initialRaceStart.getTime(), userlogin: user.login} );
        socket.join('activeRacers');
        //socket.broadcast.to('activeRacers').emit('raceStartsIn', initialRaceStart );
        activeUsers.add(user.login);
      } else {
        socket.emit('waitRace', raceEnds.getTime());
      }
    }

    socket.on('initiateStartRace', payload => {
      raceEnds = new Date();
      raceEnds.setMinutes(raceEnds.getMinutes() + 1);
      socket.emit('startRace', {activeUsers: Array.from(activeUsers), raceEnds: raceEnds.getTime()});
      facade({activeUsers, socket});
    });

    socket.on('emitUserInfo', payload => {
      facade({payload, socket, raceEnds});
      socket.broadcast.to('activeRacers').emit('updateUsers', payload );
    });

    socket.on('initiateEndRace', payload => {
      socket.broadcast.to('activeRacers').emit('endRace' );
    });
  });
});