const cars = require('./cars.json');
const _ = require('lodash');

    function getGreeting(racers) {
         let initialGreetings = "На вулиці зараз трохи пасмурно, але на Львів Арена зараз просто чудова атмосфера: двигуни гарчать, глядачі посміхаються а гонщики ледь помітно нервують та готуюуть своїх залізних конів до заїзду. А коментувати це все дійстово Вам буду я, Ескейп Ентерович і я радий вас вітати зі словами Доброго Вам дня панове! А тим часом список гонщиків: ";
         _.forEach(racers, (racer, index) => {
             initialGreetings = initialGreetings + `${racer} на ${getCarName(index)} під номером ${index+1}.`;
         });
        return initialGreetings;
    }

    function getCarName(index) {
        return cars[index];
    }

    function getUpdate(users) {
         let updateInfo = '';
        _.forEach(users, (user) => {
            updateInfo = updateInfo + `${user.name}. `;
        });
        return updateInfo;
    }
    function getUpdateEvery30Seconds(users) {
        return 'Отже, станом на зараз, гонщики їдуть в такій послідовності: ' + getUpdate(users);
    }

    function get30SymbolsBeforeFinish(users) {
        return 'Фініш вже от от, і гонщики мають наступну послідовність: ' + getUpdate(users);
    }

    function getFinish(name) {
        return `І нарешті фінішну пряму пересікає ${name}`;
    }

module.exports = {getGreeting, getUpdateEvery30Seconds, get30SymbolsBeforeFinish, getFinish};

