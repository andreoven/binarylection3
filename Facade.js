const {getGreeting} = require('./Announcer');
const {updateUsers} = require('./Watcher');
const {updateRegular} = require('./Watcher');

// facade function acts like an interface, hiding complex logic.
// To implement proxy pattern we should call functions from Watcher.js from server.js,
// which in their turn will call functions from Announcer.js.
// But we already have a facade which do this. So i suppose that facade and proxy patters are implemented.
function facade({payload = false, socket, raceEnds = false, activeUsers = false}={}) {
    if (activeUsers) {
        socket.emit('announce', getGreeting(Array.from(activeUsers)));
        socket.broadcast.to('activeRacers').emit('announce', getGreeting(Array.from(activeUsers)));
    }

    if (payload) {
        updateUsers(payload, socket);
        updateRegular(raceEnds, socket);
    }
}

module.exports = { facade };