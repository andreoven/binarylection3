const {getUpdateEvery30Seconds} = require('./Announcer');
const {get30SymbolsBeforeFinish} = require('./Announcer');
const {getFinish} = require('./Announcer');
const {userFactory} = require('./Factory');

let timerInitiated = false;
let users = [];
//let userExist = false;
let finishAnnounced = false;

function updateUsers(payload, socket) {
    let userExist = false;
    for (let i = 0; i < users.length; i++) {
        if (users[i].name == payload.user) {
            users[i].symbols = payload.symbols.length;
            userExist = true;
            if (users[i].symbols == users[i].textLength){
                socket.emit('announce', getFinish(users[i].name));
                socket.broadcast.to('activeRacers').emit('announce', getFinish(users[i].name));
            }
        }
    }
    if (!userExist) {
        users.push({name: userFactory(payload).name, symbols: userFactory(payload).symbols, textLength: userFactory(payload).textLength});
    }
    if (users[0].textLength - payload.symbols.length == 30 && !finishAnnounced) {
        socket.emit('announce', get30SymbolsBeforeFinish(users));
        socket.broadcast.to('activeRacers').emit('announce', get30SymbolsBeforeFinish(users));
        finishAnnounced = true;
    }
    console.log('users', users);
}

function updateRegular(raceEnds, socket) {
    if (!timerInitiated ) {
        timerInitiated = true;
        let x = setInterval(function () {
            let now = new Date().getTime();
            let timerEnd = raceEnds.getTime() - now;
            users.sort((a,b) => (a.symbols > b.symbols) ? -1 : ((b.symbols > a.symbols) ? 1 : 0));
            if (finishAnnounced === false){
                socket.emit('announce', getUpdateEvery30Seconds(users));
                socket.broadcast.to('activeRacers').emit('announce', getUpdateEvery30Seconds(users));
            }
            if (timerEnd < 0) {
                clearInterval(x);
            }
        }, 5000);
    }
}


module.exports = {updateUsers, updateRegular};