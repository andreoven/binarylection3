const path = require('path');

module.exports = {
    entry: './public/index.js',
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'bundle.js',
        publicPath: '/public/'
    },
    watch: true,
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        ignored: /node_modules/
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            configFile: "./babel.config.js",
                            cacheDirectory: true
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {test: /\.(jpe?g|png|gif|svg)$/i, loader: "file-loader?name=app/images/[name].[ext]"},
            // {
            //   test: /\.ttf$/,
            //   use: [
            //     {
            //       loader: 'ttf-loader',
            //       options: {
            //         name: './font/[hash].[ext]',
            //       },
            //     },
            //   ]
            // }
        ]
    },
    mode: 'development',
    devServer: {
        inline: true
    },
    devtool: "source-map"
}