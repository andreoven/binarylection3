//Factory pattern.
// It's not handy to use it here, made it just for example.
function CreateUser(payload) {
    this.name = payload.user;
    this.symbols = payload.symbols.length;
    this.textLength = payload.raceLength;
}

function userFactory(payload) {
    return new CreateUser(payload);
}

module.exports = {userFactory};