
function timerBeforeRace(payload, socket, jwt) {
    let x = setInterval(function () {


        let now = new Date().getTime();

        let distance = payload.time - now;

        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((distance % (1000 * 60)) / 1000);

        document.getElementById("timer").innerHTML = `${seconds} seconds left`;

        if (distance < 0) {
            clearInterval(x);
            document.getElementById("timer").innerHTML = `Race starts now!`;
            socket.emit('initiateStartRace', jwt);
            document.getElementById("timer").innerHTML = ``;
        }
    }, 1000);
}

function timerRace(payload, socket, jwt) {
    let x = setInterval(function () {


        let now = new Date().getTime();

        let distance = payload - now;

        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((distance % (1000 * 60)) / 1000);

        document.getElementById("timer").innerHTML = `${minutes} minutes, ${seconds} seconds left`;

        if (distance < 0) {
            clearInterval(x);
            socket.emit('initiateEndRace', jwt);
            document.getElementById("timer").innerHTML = ``;

        }
    }, 1000);
}

function timerWait(payload) {
    let x = setInterval(function () {


        let now = new Date().getTime();

        let distance = payload - now;

        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((distance % (1000 * 60)) / 1000);

        document.getElementById("timer").innerHTML = `${minutes} minutes, ${seconds} seconds left`;

        if (distance < 0) {
            clearInterval(x);
            document.getElementById("timer").innerHTML = ``;

        }
    }, 1000);
}

export {timerBeforeRace, timerRace, timerWait}