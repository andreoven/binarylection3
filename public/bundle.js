/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/public/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./public/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./public/index.js":
/*!*************************!*\
  !*** ./public/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _loginUser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./loginUser */ "./public/loginUser.js");
/* harmony import */ var _timer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./timer */ "./public/timer.js");


let currentUser;
let allUsers = [];
let raceText;
const jwt = localStorage.getItem('jwt');

if (jwt) {
  const main = document.querySelector('#main');
  main.innerHTML = ``; // location.replace('/race');

  fetch('/race', {
    method: 'GET',
    headers: {
      'Authorization': `bearer ${jwt}`
    }
  }).then(res => {
    res.json().then(body => {
      raceText = body[Object.keys(body)[0]];
    });
  }).catch(err => {
    console.log('request went wrong');
  });
  const socket = io.connect('http://localhost:3000');
  socket.on('connect', function () {
    // Send ehlo event right after connect:
    socket.emit('verifyConnect', jwt);
  });
  socket.on('waitRace', payload => {
    console.log('waitRace', payload);
    Object(_timer__WEBPACK_IMPORTED_MODULE_1__["timerWait"])(payload);
  });
  socket.on('raceStartsIn', payload => {
    currentUser = payload.userlogin;
    Object(_timer__WEBPACK_IMPORTED_MODULE_1__["timerBeforeRace"])(payload, socket, jwt);
  });
  socket.on('startRace', payload => {
    document.getElementById("main").innerHTML = ``;

    for (let i = 0; i < payload.activeUsers.length; i++) {
      let linode = document.createElement("LI");
      let pnode = document.createElement("p");
      pnode.setAttribute("id", payload.activeUsers[i]);
      let progressdiv = document.createElement("div");
      let bardiv = document.createElement("div");
      progressdiv.setAttribute("id", "progress");
      bardiv.setAttribute("class", "bar");
      bardiv.setAttribute("id", `bar-${payload.activeUsers[i]}`);
      let textnode = document.createTextNode(payload.activeUsers[i]);
      pnode.appendChild(textnode);
      linode.appendChild(pnode);
      progressdiv.appendChild(bardiv);
      linode.appendChild(progressdiv);
      document.getElementById("main").appendChild(linode);
    }

    Object(_timer__WEBPACK_IMPORTED_MODULE_1__["timerRace"])(payload.raceEnds, socket, jwt);
    document.getElementById(currentUser).innerHTML = `<strong>${currentUser}</strong>`;
    let symbolsCount = [];
    let barWidth;
    window.addEventListener("keyup", event => {
      symbolsCount.push(event.key);

      if (symbolsCount.length > 0) {
        let underline = raceText.slice(0, symbolsCount.length);
        let notUnderline = raceText.slice(symbolsCount.length);
        document.getElementById("text").innerHTML = `
                <p><u>${underline}</u>${notUnderline}</p>
                `;
        barWidth = symbolsCount.length / raceText.length * 100;

        if (barWidth <= 100) {
          document.getElementById(`bar-${currentUser}`).style.width = `${barWidth}%`;
        }

        let userExist = false;

        for (let i = 0; i < allUsers.length; i++) {
          if (allUsers[i].name == currentUser) {
            allUsers[i].symbols = symbolsCount.length;
            userExist = true;
          }
        }

        if (!userExist) {
          allUsers.push({
            name: currentUser,
            symbols: symbolsCount.length
          });
        }
      }

      socket.emit('emitUserInfo', {
        user: currentUser,
        symbols: symbolsCount,
        raceLength: raceText.length
      });
    });
    document.getElementById("text").innerHTML = `
            <p>${raceText}</p>
            `;
  });
  socket.on('announce', payload => {
    document.getElementById('announcer').innerHTML = `<p>${payload}</p>`;
  });
  socket.on('updateUsers', payload => {
    let updateBarWidth = payload.symbols.length / raceText.length * 100;

    if (updateBarWidth <= 100) {
      document.getElementById(`bar-${payload.user}`).style.width = `${updateBarWidth}%`;
    }

    let userExist = false;

    for (let i = 0; i < allUsers.length; i++) {
      if (allUsers[i].name == payload.user) {
        allUsers[i].symbols = payload.symbols.length;
        userExist = true;
      }
    }

    if (!userExist) {
      allUsers.push({
        name: payload.user,
        symbols: payload.symbols.length
      });
    }
  });
  socket.on('endRace', payload => {
    document.getElementById("text").innerHTML = ``;
    document.getElementById("timer").innerHTML = ``;
    document.getElementById("main").innerHTML = ``;

    function compare(a, b) {
      if (a.symbols < b.symbols) {
        return 1;
      }

      if (a.symbols > b.symbols) {
        return -1;
      }

      return 0;
    }

    allUsers.sort(compare);
    let pnode = document.createElement("h1");
    let textnode = document.createTextNode('Congratulations!');
    pnode.appendChild(textnode);
    document.getElementById("main").appendChild(pnode);

    for (let i = 0; i < allUsers.length; i++) {
      let text = allUsers[i].name;
      let pnode = document.createElement("p");
      let textnode = document.createTextNode(text);
      pnode.appendChild(textnode);
      document.getElementById("main").appendChild(pnode);
    }
  });
} else {
  Object(_loginUser__WEBPACK_IMPORTED_MODULE_0__["loginUser"])();
}

/***/ }),

/***/ "./public/loginUser.js":
/*!*****************************!*\
  !*** ./public/loginUser.js ***!
  \*****************************/
/*! exports provided: loginUser */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loginUser", function() { return loginUser; });
function loginUser() {
  const main = document.querySelector('#main');
  main.innerHTML = `<h2>Who are you?</h2>
        <input type="text" id="login-field" placeholder="login" />
        <input type="password" id="pw-field" placeholder="password" />
        <button id="submit-btn">Log me in!</button>`;
  const loginBtn = document.querySelector('#submit-btn');
  const loginField = document.querySelector('#login-field');
  const pwField = document.querySelector('#pw-field');
  loginBtn.addEventListener('click', ev => {
    fetch('/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        login: loginField.value,
        password: pwField.value
      })
    }).then(res => {
      res.json().then(body => {
        console.log(body);

        if (body.auth) {
          localStorage.setItem('jwt', body.token);
          location.reload();
        } else {
          console.log('auth failed');
        }
      });
    }).catch(err => {
      console.log('request went wrong');
    });
  });
}



/***/ }),

/***/ "./public/timer.js":
/*!*************************!*\
  !*** ./public/timer.js ***!
  \*************************/
/*! exports provided: timerBeforeRace, timerRace, timerWait */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "timerBeforeRace", function() { return timerBeforeRace; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "timerRace", function() { return timerRace; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "timerWait", function() { return timerWait; });
function timerBeforeRace(payload, socket, jwt) {
  let x = setInterval(function () {
    let now = new Date().getTime();
    let distance = payload.time - now;
    let minutes = Math.floor(distance % (1000 * 60 * 60) / (1000 * 60));
    let seconds = Math.floor(distance % (1000 * 60) / 1000);
    document.getElementById("timer").innerHTML = `${seconds} seconds left`;

    if (distance < 0) {
      clearInterval(x);
      document.getElementById("timer").innerHTML = `Race starts now!`;
      socket.emit('initiateStartRace', jwt);
      document.getElementById("timer").innerHTML = ``;
    }
  }, 1000);
}

function timerRace(payload, socket, jwt) {
  let x = setInterval(function () {
    let now = new Date().getTime();
    let distance = payload - now;
    let minutes = Math.floor(distance % (1000 * 60 * 60) / (1000 * 60));
    let seconds = Math.floor(distance % (1000 * 60) / 1000);
    document.getElementById("timer").innerHTML = `${minutes} minutes, ${seconds} seconds left`;

    if (distance < 0) {
      clearInterval(x);
      socket.emit('initiateEndRace', jwt);
      document.getElementById("timer").innerHTML = ``;
    }
  }, 1000);
}

function timerWait(payload) {
  let x = setInterval(function () {
    let now = new Date().getTime();
    let distance = payload - now;
    let minutes = Math.floor(distance % (1000 * 60 * 60) / (1000 * 60));
    let seconds = Math.floor(distance % (1000 * 60) / 1000);
    document.getElementById("timer").innerHTML = `${minutes} minutes, ${seconds} seconds left`;

    if (distance < 0) {
      clearInterval(x);
      document.getElementById("timer").innerHTML = ``;
    }
  }, 1000);
}



/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map