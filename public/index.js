import  {loginUser}  from './loginUser';
import {timerBeforeRace, timerRace, timerWait} from './timer';

    let currentUser;
    let allUsers = [];
    let raceText;
    const jwt = localStorage.getItem('jwt');
    if (jwt) {
        const main = document.querySelector('#main');
        main.innerHTML = ``;

        // location.replace('/race');
        fetch('/race', {
            method: 'GET',
            headers: {
                'Authorization': `bearer ${jwt}`
            }
        }).then(res => {
            res.json().then(body => {
                raceText = body[Object.keys(body)[0]];

            })
        })
            .catch(err => {
                console.log('request went wrong');
            });

        const socket = io.connect('http://localhost:3000');
        socket.on('connect', function () {
            // Send ehlo event right after connect:
            socket.emit('verifyConnect', jwt);

        });

        socket.on('waitRace', payload => {
            console.log('waitRace', payload);
            timerWait(payload);
        });

        socket.on('raceStartsIn', payload => {
            currentUser = payload.userlogin;
            timerBeforeRace(payload, socket, jwt);

        });

        socket.on('startRace', payload => {
            document.getElementById("main").innerHTML = ``;
            for (let i = 0; i < payload.activeUsers.length; i++) {
                let linode = document.createElement("LI");
                let pnode = document.createElement("p");
                pnode.setAttribute("id", payload.activeUsers[i]);
                let progressdiv = document.createElement("div");
                let bardiv = document.createElement("div");
                progressdiv.setAttribute("id", "progress");
                bardiv.setAttribute("class", "bar");
                bardiv.setAttribute("id", `bar-${payload.activeUsers[i]}`);
                let textnode = document.createTextNode(payload.activeUsers[i]);
                pnode.appendChild(textnode);
                linode.appendChild(pnode);
                progressdiv.appendChild(bardiv);
                linode.appendChild(progressdiv);
                document.getElementById("main").appendChild(linode);
            }
            timerRace(payload.raceEnds, socket, jwt);
            document.getElementById(currentUser).innerHTML = `<strong>${currentUser}</strong>`;
            let symbolsCount = [];
            let barWidth;
            window.addEventListener("keyup", event => {
                symbolsCount.push(event.key);
                if (symbolsCount.length > 0){
                    let underline = raceText.slice(0, symbolsCount.length);
                    let notUnderline = raceText.slice(symbolsCount.length);
                    document.getElementById("text").innerHTML = `
                <p><u>${underline}</u>${notUnderline}</p>
                `;
                    barWidth = ((symbolsCount.length / raceText.length) * 100);
                    if (barWidth <= 100) {
                        document.getElementById(`bar-${currentUser}`).style.width = `${barWidth}%`;
                    }
                    let userExist = false;
                    for (let i = 0; i < allUsers.length; i++) {
                        if (allUsers[i].name == currentUser) {
                            allUsers[i].symbols = symbolsCount.length;
                            userExist = true;
                        }
                    }
                    if (!userExist) {
                        allUsers.push({name: currentUser, symbols: symbolsCount.length});
                    }
                }
                socket.emit('emitUserInfo', {user: currentUser, symbols: symbolsCount, raceLength: raceText.length});
            });
            document.getElementById("text").innerHTML = `
            <p>${raceText}</p>
            `;
        });

        socket.on('announce', payload => {
           document.getElementById('announcer').innerHTML = `<p>${payload}</p>`;
        });

        socket.on('updateUsers', payload => {
            let updateBarWidth = ((payload.symbols.length / raceText.length) * 100);
            if (updateBarWidth <= 100) {
                document.getElementById(`bar-${payload.user}`).style.width = `${updateBarWidth}%`;
            }


            let userExist = false;
            for (let i = 0; i < allUsers.length; i++) {
                if (allUsers[i].name == payload.user) {
                    allUsers[i].symbols = payload.symbols.length;
                    userExist = true;
                }
            }
            if (!userExist) {
                allUsers.push({name: payload.user, symbols: payload.symbols.length});
            }

        });

        socket.on('endRace', payload => {
            document.getElementById("text").innerHTML = ``;
            document.getElementById("timer").innerHTML = ``;
            document.getElementById("main").innerHTML = ``;
            function compare( a, b ) {
                if ( a.symbols < b.symbols ){
                    return 1;
                }
                if ( a.symbols > b.symbols){
                    return -1;
                }
                return 0;
            }
            allUsers.sort(compare);
            let pnode = document.createElement("h1");
            let textnode = document.createTextNode('Congratulations!');
            pnode.appendChild(textnode);
            document.getElementById("main").appendChild(pnode);
            for (let i = 0; i < allUsers.length; i++) {
                let text = allUsers[i].name;
                let pnode = document.createElement("p");
                let textnode = document.createTextNode(text);
                pnode.appendChild(textnode);
                document.getElementById("main").appendChild(pnode);
            }
        });
    }else{
            loginUser();
        }




